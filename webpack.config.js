const path = require('path');
const PATHS = {
    src: path.resolve(__dirname, 'src'),
    build: path.resolve(__dirname, 'www'),
    onsenui: path.join(__dirname, 'node_modules/onsenui')
};
module.exports = {
  mode: 'production',
  entry: PATHS.src,
  output: {
    path: PATHS.build,
    filename: 'bundle.js',
  },
  devServer: {
    contentBase: PATHS.build,
    stats: 'errors-only',
    host: '0.0.0.0',
    port: 80,
    disableHostCheck: true,
  },
  module: {
    rules: [
      {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000&mimetype=application/font-woff"
      }, {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader?cacheDirectory"
        }
      }, {
          test: /\.css$/,
          loaders: ['style-loader','css-loader?url=false'],
          include: PATHS.onsenui
       }, {
        test: /\.css$/,
        loaders: ['style-loader','css-loader?url=false'],
        include: PATHS.src
      }
    ]
  }
};