import React from 'react';
import Chart from 'chart.js'

class Graph extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate(prevProps, prevState) {
        console.log("Did mount !!");
        var dateList = [];
        var tempList = [];
        var humidList = [];
        if (!this.props.response) {
            // Chưa có data thì biết làm cmj đây
        } else {
            // Có data rồi nè, convert ra array để hiển thị
            this.props.response.map((row,i) => {
                dateList.push(new Intl.DateTimeFormat('en-GB', {year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false}).format(row.created_time));
                tempList.push(row.temp);
                humidList.push(row.humid);
            });
        }  
        var ctx = document.getElementById("myChart").getContext("2d");
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: dateList,
                datasets: [{
                    label: "Temperature",
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderColor: 'rgb(255, 99, 132)',
                    borderWidth: 2,
                    data: tempList,
                    lineTension: 0
                },{
                    label: "Humidity",
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderColor: 'rgb(132, 99, 255)',
                    borderWidth: 2,
                    data: humidList,
                    lineTension: 0
                }]
            },
        
            // Configuration options go here
            options: {}
        });
    }
    render() {
        return(
            <div style={{ height: `500px`, width: '500px'}}>
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        )
    }
}
export default Graph;
