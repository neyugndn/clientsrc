import React, { Component } from 'react';
import {withGoogleMap, GoogleMap, Marker, InfoBox } from 'react-google-maps';

class Map extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var posList = this.props.response;
    var center;
    if (!posList) {
      // posList = false => tạo array, set center là điểm mặc định
      posList = [];
      center = {lat: 10.772803, lng: 106.659398}
    }
    else {
      // Chọn điểm đầu tiên làm center
      const firstPos = posList[0];
      center = {lat: parseFloat(firstPos.gpsX), lng: parseFloat(firstPos.gpsY)}
    }
    const GoogleMapExample = withGoogleMap( props => (
      <GoogleMap
        defaultCenter = {center}
        defaultZoom = {16}>
 
        {posList.map((row,i) => {
          return (
            <Marker position={{lat: parseFloat(row.gpsX), lng: parseFloat(row.gpsY)}} />
        )})}
      </GoogleMap>
    ));
    return(
       <div>
         <GoogleMapExample
           containerElement={ <div style={{ height: `500px`, width: '500px' }} /> }
           mapElement={ <div style={{ height: `100%` }} /> }
         />
       </div>
    );
    }
 };
 export default Map;