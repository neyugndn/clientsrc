import React from 'react';

class DataTable extends React.Component {
   render() {
      const status = this.props.status;
      const response = this.props.response;
      return (
        <div>
         <table id="data-table">
               <thead>
                  <tr>
                     <th colspan="6">
                     <center>
                     <h4>Latest data publishment from sensor </h4>
                     </center>
                     </th>
                  </tr>
                  <tr>
                   <th>Date</th>
                   <th>Time</th>
                   <th>X value</th>
                   <th>Y value</th>
                   <th>Humid</th>
                   <th>Temperature</th>
                 </tr>
                  </thead>
            {response
              ? <tbody>
              {response.map((row,i) =>
                 <tr>
                   <td>{new Intl.DateTimeFormat('en-GB', {year: 'numeric', month: 'numeric', day: 'numeric'}).format(row.created_time)}</td>
                   <td>{new Intl.DateTimeFormat('en-GB', {hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false}).format(row.created_time)}</td>
                   <td>{row.gpsX}</td>
                   <td>{row.gpsY}</td>
                   <td>{row.humid}</td>
                   <td>{row.temp}</td>
                 </tr>
              )}
              </tbody>
              : <tbody>
                     <tr>
                        <td colspan="6"><center>Waiting for next publishment</center></td>
                     </tr>
               </tbody>
            }
              </table>
        </div>
      );
   }
}
export default DataTable;
