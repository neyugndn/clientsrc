// Import packages
import React from 'react';
import {Tabbar, Page, Button, Card, Toolbar, ToolbarButton} from 'react-onsenui';
import io from 'socket.io-client';

// Import components
import DataTable from '../components/DataTable';
import Map from '../components/Map';
import Graph from '../components/Graph'

class Main extends React.Component {
    constructor() {
        super();
        this.state = {
             response: false,
             endpoint: "http://api.thutrongtts.ml:3000",
             status: "Disconnected"
        };
    }
    renderToolbar() {
        return (
            <Toolbar>
                <div className="center">
                    Weather Data Collector
                </div>
            </Toolbar>
        );
    }
    componentDidMount() {
        const socket = io(this.state.endpoint);
        socket.on("connect", function(){
            this.setState({status: "Connected"});
            socket.send('data');
            socket.on('broadcast', data => {
            var dfive = [];
            if (this.state.response) {
                dfive = this.state.response;
            }
            dfive.unshift(data.data_sensor);
            while (dfive.length>5) {
                dfive.pop();
            }
                this.setState({ response: dfive });
            });
        }.bind(this));
    }
    render() {
        const status = this.state.status;
        const response = this.state.response;
        return (
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <center>
                <Card>
                    <center>
                    <DataTable response={response} status={status}/>
                    <Map response={response}/>
                    <Graph response={response}/>
                    </center>
                </Card>
                </center>
            </Page>
        );
    }
}
export default Main;
