// Import react, react-dom
import React from 'react';
import { render } from 'react-dom';
// Import onsenUI css files
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';

import './css/style.css';
// Import App
import App from './App';

render(<App />, document.getElementById('app'));
