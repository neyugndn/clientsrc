import React from 'react';

import {Navigator} from 'react-onsenui';

import Layout from './Layout';

class App extends React.Component {
    renderPage(route, navigator) {
      const props = route.props || {};
      props.navigator = navigator;

      return React.createElement(route.component, props);
    }

    render() {
      return (
        <Navigator
         initialRoute={{component: Layout}}
         renderPage={this.renderPage}
        />
      );
    }
}
export default App;
