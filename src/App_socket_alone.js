import React from 'react';
import ReactDOM from 'react-dom';
import io from "socket.io-client";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            response: false,
            endpoint: "http://104.154.31.164:3000",
            status: "Disconnected"
        };
    }
    componentDidMount() {
        const socket = io(this.state.endpoint);
        socket.on("connect", function(){
            this.setState({status: "Connected"});
            socket.send('data');
            socket.on('broadcast', data => {
                this.setState({ response: data.data_sensor.gpsX });
            });
        }.bind(this));
    }
    render() {
        const status = this.state.status;
        const response = this.state.response;
        return(
            <div style={{ textAlign: "center" }}>
                Sensor connection
            {response
              ? <div>
                    <table>
                        <tr>
                            <td>Status</td>
                            <td id="status">{status}</td>
                        </tr>
                        <tr>
                            <td>Data</td>
                            <td>{response}</td>
                        </tr>
                    </table>
                </div>
              : <p>Loading...</p>}
            </div>
        );
    }
}
export default App;
