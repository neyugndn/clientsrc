import React from 'react';

import {Page} from 'react-onsenui';

import Main from './tabs/Main';

class Layout extends React.Component {
   render() {
      return (
        <Page>
          <Main navigator={this.props.navigator}/>
        </Page>
      );
   }
}
export default Layout;
