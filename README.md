# Project Thu trong truyền thông số

## Common
* [Creating a Cordova Hybrid App with React, Redux and Webpack](https://medium.com/the-web-tub/creating-a-cordova-hybrid-app-with-react-redux-and-webpack-13fe24b6b272)
* [What is NodeJS?](https://medium.freecodecamp.org/what-exactly-is-node-js-ae36e97449f5)

## Javascript
* [Bind, Apply and Call](https://viblo.asia/p/bind-apply-and-call-trong-javascript-DzVGpoMDvnW)
* [Converting an Object to an Array of Objects](https://medium.com/chrisburgin/javascript-converting-an-object-to-an-array-94b030a1604c)
* [Convert timestamp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat)

## React
* [Convert timestamp using package](https://www.npmjs.com/package/react-timestamp)
* [Drawing chart](http://www.chartjs.org/docs/latest/getting-started/installation.html)
* [Understanding React — Component life-cycle](https://medium.com/@baphemot/understanding-reactjs-component-life-cycle-823a640b3e8d)

## Setup project dir
* [How to Create a React app from scratch using Webpack 4](https://medium.freecodecamp.org/part-1-react-app-from-scratch-using-webpack-4-562b1d231e75)
* [Setting up a React project from scratch](https://codeburst.io/setting-up-a-react-project-from-scratch-d62f38ab6d97)
* [Tutorial: How to set up React, webpack 4, and Babel 7 (2018)](https://www.valentinog.com/blog/react-webpack-babel/)

## Troubleshooting 
* [When using react.js webpack-dev-server does not bundle](https://medium.com/bcgdv-engineering/when-using-react-js-webpack-dev-server-does-not-bundle-c2d340b0a3e8)

## Socket.io client
* [Github socket.io-client](https://github.com/socketio/socket.io-client)
* [What is Socket.io?](https://viblo.asia/p/co-ban-ve-socketio-bJzKm0kY59N)
* [Full socket.io client and server example](https://gist.github.com/luciopaiva/e6f60bd6e156714f0c5505c2be8e06d8)
* [Combining React with Socket.io for real-time goodness](https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34)
* [Going real time with Socket.IO, Node.Js and React](https://www.valentinog.com/blog/socket-io-node-js-react/#Making_use_of_our_realtime_server_the_React_client)
* [Docs](https://github.com/socketio/socket.io-client/blob/master/docs/API.md)

## OnsenUI
* [Navigation and Tabs in the Onsen UI React Extension](https://medium.com/the-web-tub/navigation-and-tabs-in-the-onsen-ui-react-extension-431347cfc372)
* [Docs](https://onsen.io/v2/api/react/)

## Bootstrap
* [Getting started](https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/getting-started.html)
* [Storybook](https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Basic%20Table&selectedStory=basic%20table&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel)
* [Docs](https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/table-props.html)

## GoogleMapAPI
* [How To Render A Map Component Using ‘react-google-maps](https://medium.com/@yelstin.fernandes/render-a-map-component-using-react-google-maps-5f7fb3e418bb)
* [How to Write a Google Maps React Component](https://www.fullstackreact.com/articles/how-to-write-a-google-maps-react-component/#)
* [google-maps-react makes adding Google Maps Api to a React app a breeze](https://itnext.io/google-maps-react-makes-adding-google-maps-api-to-a-react-app-a-breeze-effb7b89e54)
